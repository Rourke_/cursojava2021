package modulo1;

public class mod1_ejercicio2 {

	public static void main(String[] args) {
		
		//imprimir nombre, apellido, mail y telefono de compaņeros
		//los mails y telefonos son ficticios, solo estan para el ejercicio
		System.out.println("Apellido		Nombre		Mail			Telefono");
		System.out.println("_________________________________________________________________________");
		System.out.println("");
		System.out.println("Argaņaras		Manuel		manu@gmail.com		15-0000-0000");
		System.out.println("Arrosogaray		Dana		dana@gmail.com		15-0000-0001");
		System.out.println("Baliner			Ivan		ivan@gmail.com		15-0000-0010");
		System.out.println("Barros			Gabirel		gabi@gmail.com		15-0000-0011");
		System.out.println("Camacho			Maximo		maxi@gmail.com		15-0000-0100");
		
		
	}

}