package modulo1;

public class mod1_ejercicio3 {

	public static void main(String[] args) {
		
		System.out.println("Tecla de Escape			Funcion");
		System.out.println("------------------------------------------------------");
		System.out.println("");
		System.out.println("\\n				Nueva linea");
		System.out.println("\\t				Tabulacion");
		System.out.println("\\\"				Comillas");
		System.out.println("\\\\				\"\\\" dentro del texto");
		System.out.println("\\'				\" \' \" dentro del texto");
		
	}

}
