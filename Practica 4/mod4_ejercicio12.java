package modulo4;
import java.util.Scanner;

public class mod4_ejercicio12 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un numero entero: ");
		int num = scan.nextInt();
		
		if (num > 0 || num < 12)
			System.out.println("El numero ingresado pertenece a la primer docena.");
		else if (num > 12 || num < 24)
			System.out.println("El numero ingresado pertenece a la segunda docena.");
		else if (num > 24 || num < 36)
			System.out.println("El numero ingresado pertenece a la tercer docena.");
		else
			System.out.println("El numero \"" + num + "\" esta fuera de rango.");
		
		scan=null;


	}

}
