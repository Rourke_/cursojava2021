package modulo4;
import java.util.Scanner;

public class mod4_ejercicio16 {

	public static void main(String[] args) {
		
		System.out.println("La tabla de que numero quiere saber: ");
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		
		System.out.println("Tabla del " + num + ": ");
		
		for(int i = 0; i < 10; i++ )
		{
			int resultado = num * i;
			System.out.println(num + "x" + i + " = " + resultado);
		}
		
		scan=null;


	}

}
