package modulo4;
import java.util.Scanner;

public class mod4_ejercicio10 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la primera variable: ");
		int valor1 =scan.nextInt();
		System.out.println("Ingrese la segunda variable: ");
		int valor2 =scan.nextInt();
		System.out.println("Ingrese la tercera variable: ");
		int valor3 =scan.nextInt();
		
		if (valor1 > valor2 && valor2 > valor3)
			System.out.println("La primer variable \"" + valor1 + "\" es mayor.");
		else if (valor2 > valor1 && valor2 > valor3)
			System.out.println("La segunda variable \"" + valor2 + "\" es mayor.");
		else if (valor3 > valor1 && valor3 > valor2)
			System.out.println("La tercera variable \"" + valor3 + "\" es mayor.");
		else
			System.out.println("No hay un numero mayor.");

		scan=null;


	}

}
