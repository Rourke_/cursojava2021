package modulo4;
import java.util.Scanner;

public class mod4_ejercicio19 {

	public static void main(String[] args) {
		
		int cantnum = 1;
		int sum = 0;
		int prom = 0;
			
		while (cantnum<11)
		{
			int random = (int) Math.floor(Math.random()*1000);
			System.out.println("El " + cantnum + "� numero aleatorio es " + random);	
			sum = sum + random;
			cantnum = cantnum + 1;
		}
		
		System.out.println("\nLa suma de los numeros aleatorios es: " + sum);
		prom = sum/10;
		System.out.println("\nEl promedio de estas sumas es: " + prom);


	}

}
