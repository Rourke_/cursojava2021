package modulo4;
import java.util.Scanner;

public class mod4_ejercicio4 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Elija una de las siguientes categorias");
		System.out.println("A, B o C: ");
		String cat = scan.nextLine();
		
		if (cat.equals("A") || cat.equals("a"))
			System.out.println("Hijo");
		if (cat.equals("B") || cat.equals("b"))
			System.out.println("Padres");
		if (cat.equals("C") || cat.equals("c"))
			System.out.println("Abuelos");
		
		scan=null;


	}

}
