package modulo4;
import java.util.Scanner;

public class mod4_ejercicio11 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una letra: ");
		char letra = scan.next().charAt(0);
		
		if (letra == 'a' || letra == 'A' || letra == 'e'  || letra == 'E' || letra == 'i' || letra == 'I' || letra == 'o' || letra == 'O'|| letra == 'u' || letra == 'U')
			System.out.println("La letra ingresada es una vocal.");
		else
			System.out.println("La letra ingresada es una consonante.");
		
		scan=null;


	}

}
