package modulo4;
import java.util.Scanner;

public class mod4_ejercicio17 {

	public static void main(String[] args) {
		
		System.out.println("La tabla de que numero quiere saber: ");
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		int sumpares = 0;
		
		System.out.println("Tabla del " + num + ": ");
		
		for(int i=1; i<11; i++ )
		{
			int resultado = num * i;
			System.out.println(num + "x" + i + " = " + resultado);
			if (resultado%2==0)
				sumpares = sumpares + resultado;
		}
		
		System.out.println("Suma de los resultados pares es " + sumpares);
		
		scan=null;


	}

}
