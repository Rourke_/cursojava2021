package modulo4;
import java.util.Scanner;

public class mod4_ejercicio14 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("�En que puesto quedo?\n(Utilice numero para referirse a su puesto) ");
		int puesto = scan.nextInt();
		
		switch (puesto)
		{
		case 1:
			System.out.println("Recibe medalla de oro.");
			break;
		case 2:
			System.out.println("Recibe medalla de plata.");
			break;
		case 3:
			System.out.println("Recibe medalla de bronce.");
			break;
		default:
			System.out.println("No recibe ninguna medalla, gracias por el esfuerzo.");
			break;
		}
		
		scan=null;


	}

}
