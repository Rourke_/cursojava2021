package modulo4;
import java.util.Scanner;

public class mod4_ejercicio18 {

	public static void main(String[] args) {
		
		System.out.println("Escriba \"123\" para visualizar todas las tablas");
		Scanner scan = new Scanner(System.in);
		int num = 10;
		int java = scan.nextInt();
		
		if (java == 123)
			for(int o=0; o<num+1; o++ )
			{
				System.out.println("Tabla del " + o);
				for(int i=1; i<11; i++ )
				{
					int resultado = o * i;
					System.out.println(o + "x" + i + " = " + resultado);
				}
				System.out.println("");
			
			}
		else 
			System.out.println("Usted no ingreso \"123\", usted no quiere visualizar las tablas.");
		scan=null;
	}

}
