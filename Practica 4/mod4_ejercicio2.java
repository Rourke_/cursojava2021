package modulo4;
import java.util.Scanner;

public class mod4_ejercicio2 {

	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un numero entero: ");
		int num = scan.nextInt();
		
		if (num%2==0)
			System.out.println("El numero es par.");
		else
			System.out.println("El numero es impar.");
		
		scan=null;
	}

}
