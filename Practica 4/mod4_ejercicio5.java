package modulo4;
import java.util.Scanner;

public class mod4_ejercicio5 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("�En que puesto quedo? ");
		System.out.println("(Utilice numero para referirse a su puesto) ");
		int puesto =scan.nextInt();
		
		if(puesto == 1)
			System.out.println("Recibe la medalla de oro.");
		else if(puesto == 2)
			System.out.println("Recibe la medalla de plata.");
		else if(puesto == 3)
			System.out.println("Recibe la medalla de bronce.");
		else
			System.out.println("No recibe ninguna medalla, gracias por el esfuerzo.");
		
		scan=null;


	}

}
