package modulo4;
import java.util.Scanner;

public class mod4_ejercicio3 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un mes: ");
		String grupo = scan.nextLine();
		
		if (grupo.equals("Abril") || grupo.equals("abril") || grupo.equals("Junio") || grupo.equals("junio") || grupo.equals("Septiembre") || grupo.equals("septiembre") || grupo.equals("Noviembre") || grupo.equals("noviembre"))
			System.out.println("El mes tiene 30 dias.");
		else if (grupo.equals("Enero") || grupo.equals("enero") || grupo.equals("Marzo") || grupo.equals("marzo") || grupo.equals("Julio") || grupo.equals("julio") || grupo.equals("Agosto") || grupo.equals("agosto") || grupo.equals("Octubre") || grupo.equals("octubre") || grupo.equals("Diciembre") || grupo.equals("diciembre"))
			System.out.println("El mes tiene 31 dias.");
		else if (grupo.equals("Febrero") || grupo.equals("febrero"))
			System.out.println("El mes tiene 28 dias.");
		else
			System.out.println("No ingreso un mes o lo ingreso mal");
		
		scan=null;

		
	}

}
