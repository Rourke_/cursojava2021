package modulo4;
import java.util.Scanner;

public class mod4_ejercicio15 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("De que tipo de auto quiere saber �A, B o C? ");
		char tipo = scan.next().charAt(0);
		
		switch (tipo)
		{
		case 'A':
			System.out.println("Este tipo de auto tienen 4 ruedas y un motor.");
			break;
		case 'B':
			System.out.println("Este tipo de auto tienen 4 ruedas, un motor, cierre centralizado y aire.");
			break;
		case 'C':
			System.out.println("Este tipo de auto tienen 4 ruedas, un motor, cierre centralizado, aire y airbag.");
			break;
		default:
			System.out.println("El tipo que ingreso no existe.");
			break;
		}
		
		scan=null;


	}

}
