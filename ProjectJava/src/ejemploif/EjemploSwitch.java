package ejemploif;

import java.util.Scanner;

public class EjemploSwitch {
	
	public static void main (String[] args){
		System.out.println("Ingrese como salio en la competencia ");
		Scanner scan = new Scanner (System.in);
		
		int position = scan.nextInt();
		
		switch (position) {
		
		case 1:
			System.out.println("Te pasaste macho sos el primero");
			break;
		case 2:
			System.out.println("Subcampeon medalla de plata...");
			break;
		case 3:
			System.out.println("Medalla de Bronce, suerte para la next");
			break;
		
		default:
			System.out.println("Siga participando");
			break;
		}
	}

}
