package ejemploif;

import java.util.Scanner;

public class Ejemploif {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese tres notas ");
		
		float nota1 = scan.nextFloat();
		float nota2 = scan.nextFloat();
		float nota3 = scan.nextFloat();
		
		float promedio = (nota1+nota2+nota3)/3;
		
		System.out.println("Las primeras notas fueron"+ nota1);
		System.out.println("Las segundas notas fueron"+ nota2);
		System.out.println("Las terceras notas fueron"+ nota3);
		
		if(promedio >=7)
			System.out.println("Aprobo feliz con un promedio de "+ promedio);
		else
			System.out.println("Aprobo triste con un promedio de "+ promedio);
		
		scan=null;
	}

}
