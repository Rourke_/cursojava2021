package ejemploif;

import java.util.Scanner;

public class EjemploCiclos {
	
	public static void main(String[] args){
		
		System.out.println("Ingrese una tabla  a evaluar ");
		Scanner scan = new Scanner(System.in);
		
		int tabla = scan.nextInt();
		
		System.out.println("La tabla a mostrar es " + tabla);
		//inicio; condicion; incremente i = i + 1
		
		for(int i=0; i<10; i++ ){
			int result = tabla * i;
			System.out.println(tabla + "x" + i + "=" + result);
			
		}
	}

}
